# OVERLAYS
* An overlay means taking the predefined functionality and imposing your own definitions over that (to customize the standard functionality).

* In a standard instance the predefined functionality is held under /libs and it is recommended practice to define your overlay (customizations) under the /apps branch. AEM uses a search path to find a resource, searching first the /apps branch and then the /libs branch (the search path can be configured in /etc).

In this Document we will see how to __Create Custom Console in AEM Using Overlay and Coral ui.__


To achieve this we will cover following steps
-----

[Creating a Custom cq node and integrating with AEM Console.](#some-id)


[Creating a Custom Console to perform  Operation on the Page.](#console-id)

[Integrating the Custom Rendering Pages.](#custom-id)


# <a name="some-id"></a> creating a Custom cq node and integrating with AEM Console.
-----
- Go to [CRXDE Lite](http://localhost:4502/crx/de/index.jsp) console.
- Create a node structure in apps.
- cq folder is hardcoded if we want to create custom console in AEM compulsory we should follow the below structure.


## OverlayNode
 ![](Image/Capture.JPG)
    
- cq (Its a hardcoded folder for aem console)

| Name             | Type                | Value       |
| ---------------- |:-------------------:|------------:|
| jcr:primaryType  | Name                | sling:Folder|

+ core (custom console Should be included under core)
   
| Name             | Type                | Value       |
| ---------------- |:-------------------:|------------:|
| jcr:primaryType  | Name                | nt:folder   |

- content 

| Name             | Type                | Value              |
| ---------------- |:-------------------:|-------------------:|
| jcr:primaryType  | Name                | sling:OrderedFolder|

- nav and tools (It will help to include your custom console)

| Name             | Type                | Value              |
| ---------------- |:-------------------:|-------------------:|
| jcr:primaryType  | Name                | nt:unstructured    |

- Training Management

| Name             | Type                | Value              |
| ---------------- |:-------------------:|-------------------:|
| jcr:mixinTypes  | Name                | rep:AccessControllable|
| jcr:primaryType | Name                | nt:unstructured     |

- courses

## courses property
 ![](Image/courses_property.JPG)

- rep:policy

| Name             | Type                | Value              |
| ---------------- |:-------------------:|-------------------:|
| jcr:primaryType  | Name                | rep:ACL            |

1. deny

| Name             | Type                | Value              |
| ---------------- |:-------------------:|-------------------:|
| jcr:primaryType  | Name                | rep:DenyACE        |
| rep:principalName | String             | everyone           |
| rep:privileges   | Name[]              | jcr:all            |

2. allow1

| Name             | Type                | Value              |
| ---------------- |:-------------------:|-------------------:|
| jcr:primaryType  | Name                | rep:GrantACE       |
| rep:principalName | String             | administrator      |
| rep:privileges   | Name[]              | jcr:all            |

  xssprotection (/libs/cq/xssprotection/config.xml)
  (https://helpx.adobe.com/experience-manager/6-3/sites/developing/using/security.html)
---------------------
- xssprotection 

| Name             | Type                | Value              |
| ---------------- |:-------------------:|-------------------:|
| jcr:primaryType  | Name                | sling:Folder      |
| jcr:mixinTypes  | Name                | rep:AccessControllable|

 - rep:policy

| Name             | Type                | Value              |
| ---------------- |:-------------------:|-------------------:|
| jcr:primaryType  | Name                | rep:GrantACE       | 
| rep:principalName | String             | sling-xss          |
| rep:privileges   | Name[]              | jcr:read            |

Output after following Above steps.


## AEM console 
 ![](Image/console.JPG)



# <a name="console-id"></a> Creating a Custom Console to perform  Operation on the Page.
---------
- create rendering page of console under libs folder the components will contain html,js and css where as, content will contain cq:page and render over components node using sling:resourceType.
- In components will create node which contains html page.
- In  content we will create a cq:page and render the html page of components, For each cq:page we will create a individual node. 
## inHouseProjectStructure 
 ![](Image/inHouseProject.JPG)

1. create inHouseProject under (/libs/cq/inHouseProject) with nt:folder.
2. create components and content.

> For Detail Information follow the **sling:resourceType** of Each node.

- components (It will contains node and clientlib which contains Html page).

| Name             | Type                | Value       |
| ---------------- |:-------------------:|------------:|
| jcr:primaryType  | Name                | sling:Folder|

- admin (It contains html pages).

| Name             | Type                | Value              |
| ---------------- |:-------------------:|-------------------:|
| jcr:primaryType  | Name                | nt:unstructured    |

-----------
- content(It will contain cq:page which will render on components html pages).

## AEM console 
 ![](Image/content.JPG)

 - content

| Name             | Type                | Value       |
| ---------------- |:-------------------:|------------:|
| jcr:primaryType  | Name                | sling:Folder|

- console

| Name             | Type                | Value       |
| ---------------- |:-------------------:|------------:|
| jcr:primaryType  | Name                | cq:page     |

- console/jcr:content

| Name             | Type                | Value       |
| ---------------- |:-------------------:|------------:|
| sling:resourceType  | String           | granite/ui/components/shell/page |

+ head (Its a nt:unstructure)


It will contain the head tags of the overlays page it will contain tags like viewport,meta,clientlibs and favicon.

    - viewport (viewport of webpage)

| Name             | Type                | Value       |
| ---------------- |:-------------------:|------------:|
| sling:resourceType | String            | granite/ui/components/coral/foundation/admin/page/viewport     |

    - meta (It contains metatags)

| Name             | Type                | Value       |
| ---------------- |:-------------------:|------------:|
|content           | String            | chrome=1      |
|name              | String            | X-UA-Compatible |
|sling:resourceType | String           | granite/ui/components/coral/foundation/meta     |

    - favicon

| Name             | Type                | Value       |
| ---------------- |:-------------------:|------------:|
| sling:resourceType | String            | granite/ui/components/coral/foundation/page/favicon    | 

* title

| Name             | Type                | Value       |
| ---------------- |:-------------------:|------------:|
| sling:resourceType | String            | granite/ui/components/shell/title    | 


> content contains node in which html code is written

* content (content contains the rendering node of the page)

| Name             | Type                | Value       |
| ---------------- |:-------------------:|------------:|
| sling:resourceType | String            | /libs/cq/inHouseProject/components/admin  |

 
It will contains the **sling:resourceType** of  admin.html of components node in libs/cq/inHouseProject. 
-------

# <a name="custom-id"></a>Integrating the Custom Rendering Pages.

- In /apps/cq/core/content/nav/tools/Training Management/courses
we will provide the **href /libs/cq/inHouseProject/content/console.html** of the cq:page(/libs/cq/inHouseProject/content/console) which will  render the html page.

- Goto **/libs/cq/inHouseProject/content/console** then goto (/libs/cq/inHouseProject/content/console/jcr:content/body/content/content/items/admin)

- In **/libs/cq/inHouseProject/content/console/jcr:content/content** we will provide the reference of the page where html code is written **sling:resourceType                    cq/inHouseProject/components/admin** 

- Html code is written under **/libs/cq/inHouseProject/components/admin/admin.html**
path were you will write code. 
















-----
### Reference Links
------- 

1. (https://helpx.adobe.com/experience-manager/6-3/sites/developing/using/overlays.html)

2.  (https://helpx.adobe.com/experience-manager/6-3/sites/developing/using/customizing-consoles-touch.html#CreateaCustomConsole)

3. ( https://helpx.adobe.com/experience-manager/6-3/sites/developing/using/customizing-page-authoring-touch.html#AddNewSelectionCategorytoAssetBrowser)

4. (https://helpx.adobe.com/experience-manager/6-3/sites/developing/using/reference-materials/coral-ui/coralui3/getting-started.html)

5. (https://helpx.adobe.com/experience-manager/6-3/sites/developing/using/reference-materials/coral-ui/coralui3/components.html)